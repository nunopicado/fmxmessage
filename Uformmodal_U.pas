unit Uformmodal_U;

interface

Uses FMX.Forms, FMX.Graphics, FMX.Objects, System.Classes, FMX.Ani, FMX.Effects;

type
  TFormModal = class(TForm)

  Private
  Class var
    FInstance: TFormModal;

    FFormModal: TForm;

    FModal: Trectangle;
    FMensagem: Trectangle;

    FAnimacao: TFloatAnimation;
    FSombra: TShadowEffect;

    Procedure Finish(Sender: Tobject);
    { Private declarations }
  public
    FFormDestino: TForm;
    FFormModalCom: TComponentClass;

    Function PreparaForm: Boolean;

    Procedure Show;
    Procedure Hide;

    Class Function GetInstance: TFormModal;

    { Public declarations }
  end;

implementation

Uses System.UITypes;

Procedure TFormModal.Finish(Sender: Tobject);
begin
  if FAnimacao.StartValue <> -FMensagem.Height then
  Begin
    FModal.Visible := False;
    FMensagem.Visible := False;
    FFormModal.Free;
  End;
end;

Class Function TFormModal.GetInstance: TFormModal;
begin
  if not Assigned(FInstance) then
    FInstance := TFormModal.CreateNew(nil);
  Result := FInstance;
end;

Procedure TFormModal.Hide;
begin
  FAnimacao.StartValue := Round(FFormDestino.Height / 2) -
    Round(FMensagem.Height / 2);
  FAnimacao.StopValue := -FMensagem.Height;
  FAnimacao.Start;
end;

Function TFormModal.PreparaForm: Boolean;
begin
  FModal := Trectangle.Create(FFormDestino);
  FModal.Visible := False;

  FMensagem := Trectangle.Create(FFormDestino);
  FMensagem.Visible := False;

  FModal.Parent := FFormDestino;
  FModal.Align := FMX.Types.TAlignLayout.Client;
  FModal.Opacity := 0.5;
  FModal.Fill.Color := TAlphaColorRec.Black;

  FMensagem.Parent := FFormDestino;
  FMensagem.Align := FMX.Types.TAlignLayout.VertCenter;
  FMensagem.Height := 225;
  FMensagem.Margins.Left := 10;
  FMensagem.Margins.Right := 10;
  FMensagem.XRadius := 6;
  FMensagem.YRadius := 6;
  FMensagem.Stroke.Kind := FMX.Graphics.TBrushKind.BkNone;
  FMensagem.Fill.Kind := FMX.Graphics.TBrushKind.bkGradient;
  FMensagem.Fill.Gradient.Color := $FFD4D4D4;
  FMensagem.Fill.Gradient.Color1 := $FFF4F4F4;

  FAnimacao := TFloatAnimation.Create(FFormDestino);
  FAnimacao.Parent := FMensagem;
  FAnimacao.PropertyName := 'Position.Y';
  FAnimacao.OnFinish := Finish;
  FAnimacao.Duration := 0.4;
  FSombra := TShadowEffect.Create(FFormDestino);
  FSombra.Parent := FMensagem;
  FSombra.Softness := 0.5;
  FSombra.Distance := 2;

  Result := True;
end;

Procedure TFormModal.Show;
begin
  Application.CreateForm(FFormModalCom, FFormModal);

  FModal.Visible := True;
  FMensagem.Visible := True;

  FAnimacao.StartValue := -FMensagem.Height;
  FAnimacao.StopValue := Round(FFormDestino.Height / 2) -
    Round(FMensagem.Height / 2);
  FAnimacao.Start;

  while FFormModal.ChildrenCount > 0 do
    FFormModal.Children[0].Parent := FMensagem;
end;

Initialization

Finalization

TFormModal.FInstance.Free;

end.
