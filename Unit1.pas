unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, Uformmodal_U;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    Modal: TFormModal;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses Form2_U;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Modal.Show;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Modal := TFormModal.GetInstance;
  Modal.FFormDestino := Self;
  Modal.FFormModalcom := TForm2;
  Modal.PreparaForm;
end;

end.
